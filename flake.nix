{
	inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
	};
	outputs = {self,nixpkgs}: {
    defaultPackage.aarch64-linux = 
      with import nixpkgs {system = "aarch64-linux";};
      stdenv.mkDerivation {
        name = "hello";
	      src = self;
        buildInputs = [
          gcc
        ];
	      nativeBuildInputs = [
          pkgs.pkg-config		
          # pkgs.raylib
	      ];
	      buildPhase  = ''
          	gcc -Wall -Wextra -std=c11 -o hello ./src/time_console.c
	          '';
# `pkg-config --cflags raylib`   `pkg-config --libs raylib`
	      installPhase = ''
	             mkdir -p $out/bin
               install -t $out/bin hello
       '';
      };
	};
}
